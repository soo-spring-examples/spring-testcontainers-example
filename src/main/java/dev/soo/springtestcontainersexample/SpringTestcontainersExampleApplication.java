package dev.soo.springtestcontainersexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.annotation.Id;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class SpringTestcontainersExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringTestcontainersExampleApplication.class, args);
    }

}

record Message(@Id Long id, String content) {
}

@Repository
interface MessageRepository extends CrudRepository<Message, Long> {
}

@RestController
@RequestMapping("/messages")
class MessageController {

    private final MessageRepository repository;

    MessageController(MessageRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public Iterable<Message> findAll() {
        System.out.println("trying...");
        return repository.findAll();
    }

    @GetMapping("/{id}")
    public Message findById(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(RuntimeException::new);
    }
}
