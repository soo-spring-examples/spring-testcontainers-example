create table if not exists message(
    id bigserial primary key not null,
    content varchar(255) not null
);