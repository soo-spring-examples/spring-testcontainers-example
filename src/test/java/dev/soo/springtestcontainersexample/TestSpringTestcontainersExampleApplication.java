package dev.soo.springtestcontainersexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.testcontainers.containers.PostgreSQLContainer;

@Configuration
public class TestSpringTestcontainersExampleApplication {

    @Bean
    @ServiceConnection
    PostgreSQLContainer postgreSQLContainer() {
        return new PostgreSQLContainer("postgres:15-alpine");
    }

    public static void main(String[] args) {
        SpringApplication
                .from(SpringTestcontainersExampleApplication::main)
                .with(TestSpringTestcontainersExampleApplication.class)
                .run(args);
    }
}
