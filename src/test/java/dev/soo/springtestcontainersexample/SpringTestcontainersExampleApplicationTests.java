package dev.soo.springtestcontainersexample;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@Testcontainers
class SpringTestcontainersExampleApplicationTests {

    @Container
    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:15-alpine");

    @Autowired
    private MessageRepository repository;

    @Test
    void contextLoads() {
        Iterable<Message> messages = repository.findAll();
        Collection<Message> list = new ArrayList<>();
        for (Message message : messages) {
            list.add(message);
        }

        assertEquals(6, list.size());
    }

}
